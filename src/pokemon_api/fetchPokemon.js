import React from 'react';
import axios from 'axios';

const fetchPokemon = () => {
    const api = axios.get('https://pokeapi.co/api/v2/pokemon?limit=1000&offset=0')
        .then(res => res.data.results);
    return api
}

export default fetchPokemon
