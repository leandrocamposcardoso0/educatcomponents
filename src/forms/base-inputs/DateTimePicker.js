import React from 'react';
import { InputMask } from 'primereact/inputmask';

const DateTimePicker = ({ errors, ...props }) => (
  <div className="input-type-style mr-2">
    <InputMask className={errors ? 'input-error red' : 'input-style'} mask="99:99" {...props} />
    {errors && <span className="input-helper red">{errors}</span>}
  </div>
);

export default DateTimePicker;
