import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

const LastUpdated = ({ children }) => (
  <span className="medium-silver pl-4">
    <FontAwesomeIcon icon="clock" className="mr-1" />
    {`Última atualização: ${children}`}
  </span>
);

LastUpdated.propTypes = {
  children: PropTypes.node,
};

LastUpdated.defaultProps = {
  children: null,
};

export default LastUpdated;
