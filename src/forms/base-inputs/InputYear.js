import React from 'react';
import PropTypes from 'prop-types';
import { InputMask } from 'primereact/inputmask';

const InputYear = ({ className, ...props }) => (
  <div className={`input-type-style ${className}`}>
    <div className="input-style">
      <InputMask mask="9999" {...props} />
    </div>
  </div>
);

InputYear.propTypes = {
  className: PropTypes.string,
};

InputYear.defaultProps = {
  className: '',
};

export default InputYear;
