import React from 'react';
import PropTypes from 'prop-types';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import InputMask from './InputMask';

const InputDecimal = ({ decimalPlaces, ...rest }) => {
  const numberMask = createNumberMask({
    prefix: '',
    suffix: '',
    decimalSymbol: ',',
    allowDecimal: true,
    includeThousandsSeparator: false,
    decimalLimit: decimalPlaces,
  });

  return <InputMask {...rest} mask={numberMask} />;
};

InputDecimal.propTypes = {
  decimalPlaces: PropTypes.number,
};

InputDecimal.defaultProps = {
  decimalPlaces: 2,
};

export default InputDecimal;
