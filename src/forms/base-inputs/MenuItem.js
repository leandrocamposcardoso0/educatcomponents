import React, { Fragment } from 'react';
import { get } from 'lodash';
import { NavLink } from 'react-router-dom';

export default class MenuItem extends React.Component {
    state = {
      isOpen: false,
    };

  openMenu = () => {
    const { isOpen } = this.state;
    this.setState({
      isOpen: !isOpen,
    });
  };

  render() {
    const { menuItem, collapsed } = this.props;
    const { isOpen } = this.state;
    const internalList = menuItem.items;
    // const fullURL = window.location.href;
    if (collapsed) {
      return (
        <li className="menu-element" onClick={this.openMenu} key={menuItem.menu}>
          <p className={`clickable ${isOpen ? 'active-menu' : ''}`}>
            <i className={`menu-icon fas fa-${menuItem.icon}`}/>
          </p>
        </li>
      );
    }
    return (
      <Fragment>
        <li className="menu-element" onClick={this.openMenu} key={menuItem.menu}>
          <p className={`clickable ${isOpen ? 'active-menu' : ''}`}>
            <i className={`menu-icon fas fa-${menuItem.icon}`}/>
            <span>{get(menuItem, 'menu')}</span>
            <i style={{ marginLeft: 15 }} className={`chevron fas fa-chevron-${isOpen ? 'up' : 'down'}`}/>
          </p>
          {isOpen && (
            <ul className="menu-list-internal animated fadeIn">
                {internalList.map(i => (
                  <li key={i.to}>
                    <NavLink activeClassName="active-link" to={i.to}>
                      {get(i, 'title')}
                    </NavLink>
                  </li>
                ))}
            </ul>
          )}
        </li>
      </Fragment>
    );
  }
}

MenuItem.defaultProps = {
  collapsed: false,
  menuItem: [],
};
