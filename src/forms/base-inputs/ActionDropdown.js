import React, { useState } from 'react';

import ActionDropdownAction from './ActionDropdownAction';
import ActionDropdownButton from '../../buttons/ActionDropdownButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ActionDropdown = ({
  label,
  actions,
  onClick,
  className,
}) => {
  const [open, setOpen] = useState(false);
  const icon = open ? 'chevron-up' : 'chevron-down';

  if (actions.length === 0) {
    return null;
  }

  return (
    <div className={className}>
      <ActionDropdownButton
        type="button"
        onClick={() => setOpen(!open)}
        // setTimeout is required so the onClick on ActionDropdownAction is fired
        onBlur={() => setTimeout(() => setOpen(false), 1000)}
      >
        <div className="d-flex justify-space-between">
          <span className="mr-1">{label}</span>
          <FontAwesomeIcon icon={icon} />
        </div>
      </ActionDropdownButton>
      {open && (
        <div className="position-absolute">
          {actions.map(action => (
            <ActionDropdownAction
              key={action.value}
              {...action}
              onClick={(value) => {
                onClick(value);
                setOpen(false);
              }}
            />
          ))}
        </div>
      )}
    </div>
  );
};

ActionDropdown.propTypes = {
  label: PropTypes.string,
  actions: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.string,
  })),
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

ActionDropdown.defaultProps = {
  label: 'Ações',
  actions: [],
  className: '',
};

export default styled(ActionDropdown)`
  position: relative;
  display: inline-block;
`;
