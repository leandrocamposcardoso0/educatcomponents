import ActionDropdownActionButton from '../../buttons/ActionDropdownActionButton';
import PropTypes from 'prop-types';
import React from 'react';

const ActionDropdownAction = ({
  label,
  value,
  onClick,
  ...rest
}) => (
  <div>
    <ActionDropdownActionButton
      type="button"
      onClick={() => onClick(value)}
      {...rest}
    >
      {label}
    </ActionDropdownActionButton>
  </div>
);

ActionDropdownAction.propTypes = {
  onClick: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.node.isRequired,
};

export default ActionDropdownAction;
