import PropTypes from 'prop-types';
import React from 'react';
import CreatableSelect from 'react-select/lib/Creatable';

const InputCreatableSelect = ({
  label, required, className, error, createLabel, ...rest
}) => (
  <div className={`input-type-style ${className}`}>
    <CreatableSelect
      className="dropdown-style"
      classNamePrefix="dropdown"
      isClearable
      placeholder="Buscar..."
      loadingMessage={() => 'Carregando...'}
      noOptionsMessage={() => 'Nenhum resultado encontrado'}
      formatCreateLabel={value => `${createLabel} "${value}"`}
      cacheOptions={false}
      {...rest}
    />
    {label && (
      <label
        htmlFor={label}
        className={`
            input-label-style
            ${error ? 'red' : 'dim'}
            ${required ? 'input-required' : ''}
          `}
      >
          {label}
      </label>
    )}
    {error && <span className="input-helper red">{error}</span>}
  </div>
);

InputCreatableSelect.propTypes = {
  className: PropTypes.string,
  error: PropTypes.string,
  label: PropTypes.string,
  required: PropTypes.bool,
  createLabel: PropTypes.string,
};

InputCreatableSelect.defaultProps = {
  className: 'w-100',
  error: '',
  label: '',
  required: false,
  createLabel: 'Criar',
};

export default InputCreatableSelect;
