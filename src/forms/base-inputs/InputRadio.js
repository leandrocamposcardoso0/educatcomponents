import React from 'react';

const InputRadio = ({ children, label, ...rest }) => <div className="d-flex" {...rest}><p>{label}</p>{children}</div>;

export default InputRadio;
