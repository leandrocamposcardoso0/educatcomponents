import React from 'react';
import PropTypes from 'prop-types';
import Textarea from 'react-textarea-autosize';

const InputTextArea = ({ error, help, ...props }) => (
  <div className="input-type-style">
    <div className="input-style">
      <Textarea minRows={2} {...props} />
      {error && <span className="input-helper red">{error}</span>}
    </div>
    {help && <span className="mt-1 input-helper help-text">{help}</span>}
  </div>
);

InputTextArea.propTypes = {
  error: PropTypes.string,
  help: PropTypes.string,
};

InputTextArea.defaultProps = {
  error: '',
  help: '',
};

export default InputTextArea;
