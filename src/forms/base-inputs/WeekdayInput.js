import React from 'react';
import InputAsync from './InputAsync';
import { fetchDays } from '../../../utils/fakedata';

const WeekdayInput = props => (
  <InputAsync
    defaultOptions
    placeholder="Dia(s) da semana"
    loadOptions={search => fetchDays({ search })}
    {...props}
  />
);

export default WeekdayInput;
