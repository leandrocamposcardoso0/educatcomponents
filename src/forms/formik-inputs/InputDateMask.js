import React from 'react';
import InputMask from './InputMask';

const InputDateMask = (props) => {
  const mask = [/[0-3]/, /[0-9]/, '/', /[0-1]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
  return (
    <InputMask placeholder="dd/mm/aaaa" mask={mask} {...props} />
  );
};

export default InputDateMask;
