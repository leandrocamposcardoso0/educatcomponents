/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import { useField, useFormikContext } from 'formik';
import AsyncSelect from 'react-select/lib/Async';


const InputSelectAsync = ({ label, className, ...props }) => {
  const { setFieldValue } = useFormikContext();
  const [field, meta] = useField(props);

  return (
    <div className={`input-type-style ${className}`}>
      <AsyncSelect
        {...field}
        {...props}
        isClearable
        placeholder="Buscar..."
        loadingMessage={() => 'Carregando...'}
        noOptionsMessage={() => 'Nenhum resultado encontrado'}
        cacheOptions={false}
        className={meta.error ? 'input-error red' : 'dropdown-style'}
        classNamePrefix="dropdown"
        onChange={val => setFieldValue(field.name, val)}
      />
      {label && (
        <label
          htmlFor={field.name}
          className={`input-label-style ${meta.error ? 'red' : 'dim'}`}
        >
          {label}
        </label>
      )}
      {meta.error && <span className="input-helper red">{meta.error}</span>}
    </div>
  );
};

InputSelectAsync.propTypes = {
  label: PropTypes.string,
  className: PropTypes.string,
};

InputSelectAsync.defaultProps = {
  label: null,
  className: '',
};

export default InputSelectAsync;
