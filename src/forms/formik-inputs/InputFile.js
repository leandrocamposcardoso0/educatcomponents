import React from 'react';
import { useField, useFormikContext } from 'formik';
import RawInputFile from '../base-inputs/InputFile';

const InputFile = (props) => {
  const [field] = useField(props);
  const { setFieldValue } = useFormikContext();

  const handleUploadComplete = (fileKey) => {
    setFieldValue(field.name, fileKey);
  };

  const handleDeleteFile = () => {
    setFieldValue(field.name, null);
  };

  return (
    <RawInputFile
      onUploadComplete={handleUploadComplete}
      onDeleteFile={handleDeleteFile}
      {...props}
    />
  );
};

export default InputFile;
