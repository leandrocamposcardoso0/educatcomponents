import './App.css';

import DataForm from './forms/formik-inputs/DataForm'
import Input from './forms/formik-inputs/Input'
import InputSelectAsync from './forms/formik-inputs/InputSelectAsync'

import React from 'react';
import fetchPokemon from './pokemon_api/fetchPokemon'

function App() {
  return (
    <div className="App">
      <DataForm>
       <br />
        <Input label="Input de texto" field="asd" name="asddsa" />
        <br />
        <InputSelectAsync
          className={'pokemon_select'}
          getOptionLabel={ (options) => options.name}
          getOptionValue={ (options) => options.url}
          label='Escolha um pokemon'
          loadingMessage='Carregando...'
          loadOptions={fetchPokemon}
          name="Pokemon"
          noOptionsMessage='Nenhum resultado encontrado'
          onChange={console.log('Pokemon selecionado')}
        />
      </DataForm>
    </div>
  );
}

export default App;
