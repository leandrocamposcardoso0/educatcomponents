import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const ActionButton = ({
  text,
  onClick,
  icon,
  className,
  disabled,
}) => (
  <button type="button" className={`btn-none d-flex ${className}`} onClick={onClick} disabled={disabled}>
    <div className="link-label">
      <div className="fa-icon">
        <FontAwesomeIcon spin={disabled} icon={disabled ? 'spinner' : icon} />
      </div>
      <div className="text">
        <p>{text}</p>
      </div>
    </div>
  </button>
);

ActionButton.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};

ActionButton.defaultProps = {
  text: '',
  icon: 'plus-circle',
  className: '',
  onClick: () => {},
  disabled: false,
};

export default ActionButton;
