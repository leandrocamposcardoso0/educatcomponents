import styled from 'styled-components';

export default styled.button`
  background-color:  ${(props) => props.theme.neutral.light};
  border: 1px solid  ${(props) => props.theme.neutral.light};
  color:  ${(props) => props.theme.neutral.dark};
  border-radius: 5px;
  padding: 10px 15px 10px;
  font-size: 13px;
  font-weight: 700;
  width: 200px;
  text-align: left;
  text-transform: uppercase;
  cursor: pointer;
  box-shadow:none;
  transition: all 0.7s;

  &:hover {
    box-shadow:inset 0 0 0 99999px rgba(0, 0, 0, 0.2);
  }
`;
