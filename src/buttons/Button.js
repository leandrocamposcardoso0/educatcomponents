import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

const Button = ({
  type,
  action,
  children,
  icon,
  className,
  ...rest
}) => (
  // eslint-disable-next-line react/button-has-type
  <button type={type} className={`btn btn-${action} ${className}`} {...rest}>
    {icon && <FontAwesomeIcon icon={icon} />}
    <span>
      {children}
    </span>
  </button>
);


Button.propTypes = {
  action: PropTypes.string,
  icon: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node,
  type: PropTypes.oneOf(['button', 'submit']),
};

Button.defaultProps = {
  action: 'primary',
  children: '',
  type: 'button',
  className: '',
  icon: null,
};

export default styled(Button)`
background: ${(props) => props.theme.primary};
border-radius: 8px;
font-weight: 600;
border: none;
color: white;
transition: all 0.7s;
box-shadow: none;

&:hover {
  box-shadow:inset 0 0 0 99999px rgba(0, 0, 0, 0.2);
}
`
