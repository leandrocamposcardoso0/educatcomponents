import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const DownloadActionButton = ({ link }) => (
  <a
    download
    href={link}
    className="btn btn-rounded btn-rounded-white basic-shadow"
    target="_blank"
    rel="noreferrer noopener"
  >
    <FontAwesomeIcon icon="download" />
  </a>
);

DownloadActionButton.propTypes = {
  link: PropTypes.string.isRequired,
};

export default DownloadActionButton;
