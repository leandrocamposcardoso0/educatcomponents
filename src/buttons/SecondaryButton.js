import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

const SecondaryButton = ({ onClick, label }) => (
  <Button
    icon="plus-circle"
    action="add primary"
    onClick={onClick}
    className="d-flex align-items-center font-weight-700"
  >
    <span className="text-underline text-capitalize">
      Adicionar {label}
    </span>
  </Button>
);

SecondaryButton.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default SecondaryButton;
