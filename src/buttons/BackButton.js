import Button from './Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';

const BackButton = ({ children, history, to }) => (
  <Button
    action="link"
    className="back-button"
    onClick={to ? () => history.push(to) : history.goBack}
  >
    <FontAwesomeIcon icon="chevron-left" className="mr-1" />
    {children}
  </Button>
);

BackButton.propTypes = {
  history: PropTypes.shape({
    goBack: PropTypes.func,
  }).isRequired,
  children: PropTypes.node,
  to: PropTypes.string,
};

BackButton.defaultProps = {
  children: null,
  to: null,
};

export default withRouter(BackButton);
