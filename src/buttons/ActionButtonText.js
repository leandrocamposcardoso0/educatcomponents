import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

const ActionButtonText = ({ text }) => (
  <>
    <div className="link-label">
      <div className="fa-icon">
        <FontAwesomeIcon icon={faPlusCircle} />
      </div>
      <div className="text">
        <p>{text}</p>
      </div>
    </div>
  </>
);

export default ActionButtonText;
