import styled from 'styled-components';

export default styled.button`
  padding: 15px;
  background-color: white;
  color: ${(props) => props.theme.neutral.dark};
  border: 1px solid ${(props) => props.theme.neutral.light};
  font-size: 12px;
  font-weight: 500;
  cursor: pointer;
  min-width: 200px;
  max-width: 200px;
  text-align: left;
  box-shadow:none;
  transition: all 0.7s;

  &:hover:not(:disabled) {
    box-shadow:inset 0 0 0 99999px rgba(0, 0, 0, 0.2);
  }

  &:disabled {
    cursor: no-drop;
  }
`;
