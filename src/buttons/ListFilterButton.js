import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import React from 'react';
import Button from './Button';

const ListFilterButton = ({ activeFilters, onClick, isCollapseOpen }) => {
  return (
    <Button action="secondary" onClick={onClick} className={`filter-button ${isCollapseOpen ? 'bg-gray-light' : ''}`}>
      {/* TODO: Badge with active filters number */}
      {activeFilters > 0
        ? activeFilters
        : <FontAwesomeIcon icon="sliders-h" />
      }
      <span className="ml-1">Filtros</span>
    </Button>
  );
};

ListFilterButton.propTypes = {
  activeFilters: PropTypes.number,
  onClick: PropTypes.func.isRequired,
  isCollapseOpen: PropTypes.bool,
};

ListFilterButton.defaultProps = {
  activeFilters: 0,
  isCollapseOpen: false,
};

export default ListFilterButton;
