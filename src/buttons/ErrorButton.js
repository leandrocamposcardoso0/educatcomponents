import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from './Button';

class ErrorButton extends React.Component {
  render() {
    const {
      fetchError,
    } = this.props;

    return (
      <Fragment>
        <p>
          <span className="danger">
            <FontAwesomeIcon icon="exclamation-circle" />
          </span>
          Não foi possível carregar as informações.
          <Button type="button" className="btn btn-none blue" onClick={() => fetchError()}>
            Tentar novamente
          </Button>
        </p>
      </Fragment>
    );
  }
}

ErrorButton.propTypes = {
  fetchError: PropTypes.func.isRequired,
};

export default ErrorButton;
